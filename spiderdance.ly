% Copyright © 2016 Taylor C. Richberger <taywee@gmx.com>
% Original work by Toby Fox, 2015
% Original Piano arrangement by Jester Musician, 2015
% This work is licensed under the Creative Commons
% Attribution-NonCommercial-ShareAlike 4.0 International License. To view a
% copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
% or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042,
% USA.

%\include "articulate.ly"

\version "2.18.2"
\pointAndClickOff

Intro = {
  f8[(\downbow c]) as( f) r16 ces' bes8-. bes16( as) es( f) |
  r16 c'(\downbow bes as) bes( c) es,( f) as f( es) f'~ f es16 \once \slurUp \grace c( bes as) |
  f4 g as bes |
}

IntroTrail = {
  c8[( bes]) f'( c) e( des)
}

DanceOne = \relative c'' {
  c8[ as]( f) c as'-. f16( g~ g4) |
  as8-. f16( g~ g4) as8-. f16( g~ g) des'( c des) |
}

DanceTwo = \relative c'' {
  c8[ as]( f) c as'-. bes16( g~ g4) |
  as8-. bes16( g~ g4) as8-. bes16( c~ c) des( c des) |
}

DanceEnd = \relative c'' {
  c8[( f]) bes,( as) bes16-. as8-. bes16~ bes8 as16( bes) |
}

DanceEndTwo = \relative c'' {
  ces16( bes as8) e( c) des( e) f4 |
}

Creeping = {
  r8 bes16(\downbow as) r bes( as) r8. bes16( as) r bes( as) r |
}

CreepingEndTwo = {
  as8( bes) ces16( bes8) as16 f4 r | 
}


SpiderDance = \relative c'' {
  \repeat volta 2 {
    \clef "tenor"
    \key f \minor
    \time 4/4
    \tempo 4=115
    \mark \markup { \box { A } }
    \relative c' {
      \Intro
      \IntroTrail c4 |
    }
    \mark \markup { \box { A′ } }
    \clef "bass"
    \relative c {
      \Intro
    }
  }
  \alternative {
    { \relative c { \IntroTrail c4 | } }
    { \relative c' { \clef "tenor" \IntroTrail c16 \clef "treble" des' c(\upbow des) | } }
  }
  \mark \markup { \box { B } }
  \repeat volta 2 {
    \DanceOne
  }
  \alternative {
    { \DanceTwo }
    { \DanceEnd }
  }
  \DanceEndTwo
  \mark \markup { \box { C } }
  c8\downbow as f16( as) c,( f) as8  f des16( f) as,( des) |
  des( as) as'( des,) g as f g e8 f16( e) g f e g |
  f8 es' des16( es) c( es) bes c as( bes) c4 |
  \DanceOne
  \DanceEnd
  \DanceEndTwo
  \mark \markup { \box { D } }
  \relative c'' {
    \repeat volta 2 {
      \repeat volta 2 {
        \Creeping
      }
      \alternative {
        { as8\prall f f( e16 f~ f4) r | }
        { \CreepingEndTwo }
      }
    }
  }
  \clef "tenor"
  \mark \markup { \box { D′ } }
  \relative c' {
    \repeat volta 2 {
      \Creeping
      as8\prall f f e16( f~ f4) \repeat tremolo 4 { as32 f32 } |
      \Creeping
    }
    \alternative {
      { \CreepingEndTwo }
      { \clef "tenor" c8 c e16( c8 bes16) c4 \appoggiatura { e16 f } g4_\markup { \right-align \epsfile #Y #10 #"Muffet.eps" } | }
    }
  }
  \bar ":|]"
}

\book {
  \header {
    copyright = "Toby Fox © 2015"
    title = "Spider Dance"
    subtitle = "Undertale"
    instrument = "violoncello"
    arranger = "Taylor C. Richberger"
    composer = "Toby Fox"
    tagline = \markup { "Original arrangement by Jester Musician. " \epsfile #Y #4 #"by-nc-sa.eps" }
  }

  % midi score
  \score {
    \new Staff {
      \new Voice {
        \set Staff.midiInstrument = #"cello"
        \unfoldRepeats \SpiderDance
      }
    }
    \midi {}
  }

  % sheet score
  \score {
    \SpiderDance
    \layout {}
  }
}

